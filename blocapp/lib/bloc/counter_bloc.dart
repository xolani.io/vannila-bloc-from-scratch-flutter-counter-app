import 'dart:async';


import 'package:blocapp/events/counter_event.dart';

class CounterBloc{
  int _counter = 0;

  /* The StreamController has an input(StreamSink)and output(Stream,
  on both ends and you pass in an Object "<int>" in our case*/
  final _counterStateController = StreamController<int>();
  // This is the input 
  StreamSink<int> get _inCounter =>  _counterStateController.sink;
  // this is the output 
  Stream<int> get outCounter => _counterStateController.stream;
  
  /*This stream will be used for passing events,
  therefore we only need a sink to listen,
  recieve event/input from the UI to process*/
  final _counterEventController = StreamController<CounterEvent>();
  //StreamContorler Sink to catch events from the UI
  Sink<CounterEvent> get counterEventSink => _counterEventController.sink;

  CounterBloc(){
    /* In our constructor we shall map our new event to a new state change
    Using the counter event controller stream we listen for events,
    Then change state as per event*/
    _counterEventController.stream.listen(_mapEventToState);

  }

  /* This is the method that maps our event to state, 
  when theres a new event, we made it private, 
  as only the our bloc StreamController needs access to it*/
  void _mapEventToState(CounterEvent event){
    if (event is IncrementEvent){
      _counter++;
    }

    else {
      _counter--;
    }

    _inCounter.add(_counter);
  }

  void dispose(){
    _counterStateController.close();
    _counterEventController.close();
  }
}