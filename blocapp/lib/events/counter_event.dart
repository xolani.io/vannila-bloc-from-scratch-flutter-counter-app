// This is our base event class 
abstract class CounterEvent {}

// These events extend our base event class with different impelementations as pe event requirement 
class IncrementEvent extends CounterEvent {}

class DecrementEvent extends CounterEvent {}
